
class ControllerDefect {

    constructor() {
        this.defects = [];
    }

    save(defects) {
        localStorage.setItem('defects', JSON.stringify(defects));
    }

    get() {
        this.defects = JSON.parse(localStorage.getItem('defects')) || [];
        return this.defects;
    }
    getById(id) {
        this.defects = this.get();
       
        const defect = this.defects.filter( def => parseInt(def.id) === parseInt(id) );

        return defect[0];
    }
    update(defect) {
        console.log(defect);

        this.defects = this.get();
       
        let newdefects = this.defects.filter( def => def.id !== defect.id );
        newdefects.push(defect);

        localStorage.clear();
        this.save(newdefects);
    
    }

    
}

export default new ControllerDefect;