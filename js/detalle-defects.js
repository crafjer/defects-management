import Controller from './ControllerDefect.js';

//varibles;
const bodyTable  = document.querySelector('.table tbody'); 

let defects = [];

const getSeverity = (severity) => {
    if (severity === 'min') {
        return `<span class="text-warning">Mínimo</span>`;
    }
    if (severity === 'critic') {
        return `<span style="color: red;">Crítico</span>`;
    }
    if (severity === 'normal') {
        return `<span class="text-info text-bolder">Normal</span>`;
    }
    if (severity === 'blocker') {
        return `<span class="text-danger">Bloqueador</span>`;
    }
}

const getStatus = (status) => {
    if (status === 'open') {
        return `<span class="badge bg-primary">abierto</span>`;
    }
    if (status === 'in_progress') {
        return `<span class="badge bg-warning">En progeso</span>`;
    }
    if (status === 'in_qa') {
        return `<span class="badge bg-info">En QA</span>`;
    }
    if (status === 'in_test') {
        return `<span class="badge bg-primary">En prueba</span>`;
    }
    if (status === 'closed') {
        return `<span class="badge bg-success">cerrado</span>`;
    }
}

const getEvidence = (evidence) => {
    let url = window.location.href;
    url = url.split('/');
    url.pop();
    url = url.join('/');
    return `${url}/${evidence}`;
}

const showDefects = () => {
    //clean table
    while(bodyTable.firstChild) {
        bodyTable.removeChild(bodyTable.firstChild);
    }

    if(defects.length > 0) {
        defects.forEach(defect => {
            const row = document.createElement('tr');

            const {id, name, developer, severity, status} = defect;

            const severityHtml = getSeverity(severity);
            const statusHtml = getStatus(status);
        
            row.innerHTML = `
                <th scope="row">${id}</th>
                <td>${name}</td>
                <td>${developer}</td>
                <td>${severityHtml}</td>
                <td><a href="edit.html?id=${id}">${statusHtml}</a></td>
            `;
            //link.appendChild(row);
            bodyTable.appendChild(row);
        })
    }


}

document.addEventListener('DOMContentLoaded', () => {

    defects = Controller.get();
    showDefects()
});
