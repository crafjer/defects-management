import ControllerDefect from './ControllerDefect.js';

//variables
const nameInput = document.querySelector('#name');
const developerInput = document.querySelector('#developer');
const descriptionInput = document.querySelector('#description');
const severityInput = document.querySelector('#severity');
const statusInput = document.querySelector('#status');
const evidenceInput = document.querySelector('#evidence');
const commentInput = document.querySelector('#comment');
const idInput = document.querySelector('#id');
const form = document.querySelector('#form');

const divAlert = document.querySelector('#alert');

let errors = [];

let defect = undefined;

const showMessage = (messages, type = undefined) => {

    const alert = document.createElement('div');
    alert.setAttribute('role', 'alert');

    if (!(type == undefined)) {
        alert.classList.add('alert-success', 'alert');
    } else {
        alert.classList.add('alert-danger', 'alert');
    }

    messages.forEach( message => {
        const element = document.createElement('span');
        element.className = 'd-block'
        element.textContent = message.body;
        alert.appendChild(element);
    });

    
    divAlert.appendChild(alert);

    if (!(type == undefined)) { 
        setTimeout(() => {
            alert.remove();
            window.location.href = 'detalle-defects.html';
        }, 3000);
    }
}


const getId = () => {
    let url = window.location.href.split('?');
    let id = url[url.length -1];
    id = id.split('=');
    id = id[id.length -1];
    return id;
}

const fillForm = defect => {
    const {id, name, developer, description, severity, evidence, status, comments} = defect;

    nameInput.value = name;
    developerInput.value = developer;
    descriptionInput.value = description;
    severityInput.value = severity;
    evidenceInput.setAttribute('src', `img/${evidence}`);
    statusInput.value = status;
    idInput.value = id;
    commentInput.value = comments;
} 

const updateDefect = e => {
    e.preventDefault();

     //clean errors
     while(divAlert.firstElementChild) {
        divAlert.removeChild(divAlert.firstElementChild);
    }
    errors = [];
    nameInput.classList.remove('bordered-red');
    developerInput.classList.remove('bordered-red');
    descriptionInput.classList.remove('bordered-red');
    severityInput.classList.remove('bordered-red');
    commentInput.classList.remove('bordered-red');
    statusInput.classList.remove('bordered-red');
    
    if (nameInput.value === '') {
        errors.push({body: 'El campo nombre del defecto es obligatorio.'});
        nameInput.classList.add('bordered-red');
    }
    if (developerInput.value === '') {
        errors.push({body: 'Debes seleccionar un desarrollador.'});
        developerInput.classList.add('bordered-red');
    }
    if (descriptionInput.value === '') {
        errors.push({body: 'El campo descripción es obligatorio.'});
        descriptionInput.classList.add('bordered-red');
    }
    if (severityInput.value === '') {
        errors.push({body: 'Debes seleccionar una Severidad.'});
        severityInput.classList.add('bordered-red');
    }
    if (commentInput.value === '') {
        errors.push({body: 'El campo comentarios es obligatorio.'});
        commentInput.classList.add('bordered-red');
    }
    if (statusInput.value === '') {
        errors.push({body: 'Debes seleccionar una estados.'});
        statusInput.classList.add('bordered-red');
    }

    if (errors.length > 0) {
        showMessage(errors)
    } else {
        defect.id = parseInt(idInput.value);
        defect.name = nameInput.value;
        defect.developer = developerInput.value;
        defect.description = descriptionInput.value;
        defect.severity = severityInput.value;
        defect.evidence = evidence;
        defect.status = statusInput.value;
        defect.comments = commentInput.value;

        ControllerDefect.update(defect);
        form.reset();
        showMessage([{body: 'El Defecto ha sido actualizado con existo.'}], 'success');
    }

}
document.addEventListener('DOMContentLoaded', () => {
    const id = getId();
    defect = ControllerDefect.getById(id);
    fillForm(defect);

    form.addEventListener('submit', updateDefect)
});