import ControllerDefect from "./ControllerDefect.js";

//variables
const nameInput = document.querySelector('#name');
const developerInput = document.querySelector('#developer');
const descriptionInput = document.querySelector('#description');
const severityInput = document.querySelector('#severity');
const evidenceInput = document.querySelector('#evidence');
const form = document.querySelector('#form');

const divAlert = document.querySelector('#alert');

let errors = [];

let defects = [];

//Funcions
const showMessage = (messages, type = undefined) => {

    const alert = document.createElement('div');
    alert.setAttribute('role', 'alert');

    if (!(type == undefined)) {
        alert.classList.add('alert-success', 'alert');
    } else {
        alert.classList.add('alert-danger', 'alert');
    }

    messages.forEach( message => {
        const element = document.createElement('span');
        element.className = 'd-block'
        element.textContent = message.body;
        alert.appendChild(element);
    });

    
    divAlert.appendChild(alert);

    if (!(type == undefined)) { 
        setTimeout(() => {
            alert.remove();
        }, 3000);
    }
}

const saveDefect = defect => {
   defects.push(defect);
   ControllerDefect.save(defects);
}

const createDefect = e => {
    e.preventDefault();

    //clean errors
    while(divAlert.firstElementChild) {
        divAlert.removeChild(divAlert.firstElementChild);
    }
    errors = [];
    
    if (nameInput.value === '') {
        errors.push({body: 'El campo nombre del defecto es obligatorio.'});
    }
    if (developerInput.value === '') {
        errors.push({body: 'Debes seleccionar un desarrollador.'});
    }
    if (descriptionInput.value === '') {
        errors.push({body: 'El campo descripción es obligatorio.'});
    }
    if (severityInput.value === '') {
        errors.push({body: 'Debes seleccionar una Severidad.'});
    }
   
    
    //get images
    let evidence = evidenceInput.value.split('\\');
    evidence = evidence[evidence.length -1];

    if (errors.length > 0) {
        showMessage(errors)
    } else {
        const defect = {
            id: Date.now(),
            name: nameInput.value,
            developer: developerInput.value,
            description: descriptionInput.value,
            severity: severityInput.value,
            evidence: evidence,
            status: 'open',
            comments: ''
        };

        saveDefect(defect);
        form.reset();
        showMessage([{body: 'El Defecto ha sido creado con existo.'}], 'success');
    }
} 

document.addEventListener('DOMContentLoaded', () => {
    defects = ControllerDefect.get();
    form.addEventListener('submit', createDefect);
});
